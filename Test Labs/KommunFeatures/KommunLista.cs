﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KommunFeatures
{
  /// <summary>
  ///     Responsible to manage a list of "Kommun" objects
  ///     Also responsible to read the list form disc (Kommunlista.csv)
  ///     See https://oppnadata.se/dataset/kommunkoder/resource/f131ad2d-74e1-455f-b60b-41a99ea03663
  ///     for more info on the dataset
  /// </summary>
  public class KommunLista
  {
    private bool _initialized;
    private List<Kommun> _kommuner;

    public KommunLista(string filename = "Kommunkoder.csv")
    {
      LoadFromFile(filename);
    }

    public int Count()
    {
      if (_initialized)
        return _kommuner.Count;
      return 0;
    }

    public Kommun Largest()
    {
      return (from k in _kommuner
              orderby k.Invanare descending
              select k).First();
    }

    public List<Kommun> SmaKommuner()
    {
      return
             _kommuner.Where(k => k.Invanare <= 20000).ToList();
    }

    public Kommun Smallest()
    {
      return (from k in _kommuner
              orderby k.Invanare descending
              select k).Last();
    }

    public void LoadFromFile(string filename = "Kommunkoder.csv")
    {
      _kommuner = new List<Kommun>();

      try
      {
        var counter = 0;
        string line;

        // Read the file and display it line by line.  
        var fs = new FileStream(filename, FileMode.Open);
        var file =
            new StreamReader(fs);
        // Read a row at a time until end of file
        while ((line = file.ReadLine()) != null)
        {
          // Skip the first line (contains column titles)
          if (counter > 0)
          {

            try
            {
              // Parse one row and create a Kommun object
              var columns = line.Split(';');
              var k = new Kommun();
              k.Kod = int.Parse(columns[0]);
              k.Namn = columns[1];
              k.Invanare = int.Parse(columns[2]);
              // Add the new Kommun object to the list
              _kommuner.Add(k);
            }
            catch (Exception e)
            {
              Console.WriteLine("{0} Exception caught.", e);
            }
          }
          counter++;
        }

        file.Dispose();

        _initialized = true;
      }
      catch (Exception e)
      {
        Console.WriteLine("Unable to load dataset.", e);
      }
    }

    public int Invanare()
    {
      var total =
      (from k in _kommuner
       select k.Invanare).Sum();
      return total;
    }

    public double Average()
    {
      return Math.Round((from k in _kommuner
                         select k.Invanare).Average());
    }

    public List<Kommun> AllStad()
    {
      return
      _kommuner.Where(k => (k.Namn.IndexOf("stad", StringComparison.OrdinalIgnoreCase) >= 0)).ToList();
    }
  }
}