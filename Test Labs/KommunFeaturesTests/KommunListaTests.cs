﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using KommunFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KommunFeatures.Tests
{
  [TestClass()]
  public class KommunListaTests
  {
    /// <summary>
    ///     Verify kommunlista on creation contains the default data file with 290 items 
    /// </summary>
    [TestMethod()]
    public void TestKommunListaInitializationA()
    {
      var kl = new KommunLista();
      Assert.AreEqual(290, kl.Count());
    }

    /// <summary>
    ///     Verify kommunlista on creation with specific data file "KommunkoderB.csv" contains 290 items 
    /// </summary>
    [TestMethod]
    public void TestKommunListaInitializationB()
    {
      var kl = new KommunLista("KommunkoderB.csv");
      Assert.AreEqual(290, kl.Count());
    }
    /// <summary>
    ///     Verify kommunlista on creation with specific data file "Kommunkoder C.csv"
    ///     has wrong items size
    /// </summary>
    [TestMethod]
    public void TestKommunListaInitializationC()
    {
      var kl = new KommunLista("Kommunkoder C.csv");
      Assert.AreNotEqual(290, kl.Count());
    }

    /// <summary>
    ///     Verify kommunlista on creation with specific data file "Kommunkoder C.csv"
    ///     has wrong items size
    /// </summary>
    [TestMethod]
    public void TestMergedFaultKommunListaInitialization()
    {
      var kl = new KommunLista("MergedFaultKommunkoder.csv");
      Assert.AreNotEqual(290, kl.Count());
    }

    /// <summary>
    ///     Verify kommunlista on creation with specific data file "Alternativkommunlista.csv" that doesnt exist
    ///  which will result to an empty list size
    /// </summary>
    [TestMethod]
    public void TestKommunListaInitializationD()
    {
      var kl = new KommunLista("Alternativkommunlista.csv");
      Assert.AreEqual(0, kl.Count());
    }

    /// <summary>
    ///     Verify that Stockholms stad is the largest city
    /// </summary>
    [TestMethod]
    public void TestFindLargest()
    {
      var kl = new KommunLista();
      var k = kl.Largest();
      Assert.AreEqual("Stockholms stad", k.Namn);
    }

    /// <summary>
    ///     Verify that Bjurholms kommun is the smallest city
    /// </summary>
    [TestMethod]
    public void TestFindSmallest()
    {
      var kl = new KommunLista();
      var k = kl.Smallest();
      Assert.AreEqual("Bjurholms kommun", k.Namn);
    }

    /// <summary>
    ///     All cities that the population is not larger than 20000
    ///     is considered as small city
    /// </summary>
    [TestMethod]
    public void TestFindSmaKommunerA()
    {
      var kl = new KommunLista();
      var smaKommuner = kl.SmaKommuner();
      foreach (var k in smaKommuner)
        Assert.IsFalse(k.Invanare > 20000);
    }

    /// <summary>
    /// Verify that the cities considered as small are 42
    /// Note: Since the code is commented than actually this test doing nothing
    /// </summary>
    [TestMethod]
    public void TestFindSmaKommunerB()
    {
      //var kl = new KommunLista();
      //var smaKommuner = kl.SmaKommuner();
      //Assert.AreEqual(42, smaKommuner.Count());
    }

    /// <summary>
    ///     Verify the total population in all cities is largest than 1000000
    /// </summary>
    [TestMethod]
    public void TestTotaltAntalInvanare()
    {
      var kl = new KommunLista();
      var total = kl.Invanare();
      Assert.IsTrue(total > 1000000);
    }

    /// <summary>
    ///     Verify the total population in all cities is largest than 1000000
    /// </summary>
    [TestMethod]
    public void TestCount()
    {
      var k = new KommunLista(null);
      Assert.IsTrue(k.Count() == 0);
    }

    /// <summary>
    ///     Verify creation of Kommun object
    /// </summary>
    [TestMethod]
    public void TestKommunObject()
    {
      var k1 = new Kommun
      {
        Invanare = 1,
        Kod = 1,
        Namn = "Test"
      };
      var k2 = new Kommun
      {
        Invanare = k1.Invanare,
        Kod = k1.Kod,
        Namn = k1.Namn
      };
      Assert.IsTrue(k1.ToString().Equals(k2.ToString()));
    }

    /// <summary>
    ///     Verify the Average population of all cities
    /// </summary>
    [TestMethod]
    public void TestAverage()
    {
      Assert.IsTrue(new KommunLista().Average().Equals(33258));
    }

    /// <summary>
    ///     Verify the Average population of all cities
    /// </summary>
    [TestMethod]
    public void TestStad()
    {
      var kommun = new KommunLista().AllStad();
      foreach (var k in kommun)
        Assert.IsTrue(k.Namn.IndexOf("stad", StringComparison.OrdinalIgnoreCase) >= 0);
    }
  }
}