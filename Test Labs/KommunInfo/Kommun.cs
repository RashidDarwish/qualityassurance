﻿namespace KommunInfo
{
    public class Kommun
    {
        private string _namn;
        private int _kod;
        private int _invanare;

        public string Namn { get => _namn; set => _namn = value; }
        public int Kod { get => _kod; set => _kod = value; }
        public int Invanare { get => _invanare; set => _invanare = value; }
    }
}
