﻿using System;
using System.Collections.Generic;
using System.IO;

namespace KommunInfo
{
    /// <summary>
    /// Responsible to manage a list of "Kommun" objects
    /// Also responsible to read the list form disc (Kommunlista.csv)
    /// See https://oppnadata.se/dataset/kommunkoder/resource/f131ad2d-74e1-455f-b60b-41a99ea03663
    /// for more info on the dataset
    /// </summary>
    public class KommunLista
    {
        private List<Kommun> _kommuner;
        private bool _initialized = false;


        public KommunLista(string filename = "Kommunkoder.csv")
        {
            LoadFromFile(filename);
        }

        public int Count()
        {
            if (_initialized)
                return _kommuner.Count;
            else
                return 0;
        }

        public void LoadFromFile(string filename = "Kommunkoder.csv")
        {
            _kommuner = new List<Kommun>();

            try
            {
                int counter = 0;
                string line;

                // Read the file and display it line by line.  
                FileStream fs = new FileStream(filename, System.IO.FileMode.Open);
                StreamReader file =
                    new StreamReader( fs);
                // Read a row at a time until end of file
                while ((line = file.ReadLine()) != null)
                {
                    // Parse one row and create a Kommun object
                    string[] columns = line.Split(',');
                    var k = new Kommun();
                    k.Kod = int.Parse(columns[0]);
                    k.Namn = columns[1];
                    k.Invanare = int.Parse(columns[2]);

                    // Add the new Kommun object to the list
                    _kommuner.Add(k);
                }

                file.Dispose();

                _initialized = true;
            }
            catch(Exception e)
            {
                throw new Exception("Unable to load dataset.", e);
            }
        }
    }    
}
