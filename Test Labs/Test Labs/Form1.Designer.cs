﻿namespace Test_Labs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Largest = new System.Windows.Forms.Button();
            this.lbl_info = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Largest
            // 
            this.btn_Largest.Location = new System.Drawing.Point(94, 205);
            this.btn_Largest.Name = "btn_Largest";
            this.btn_Largest.Size = new System.Drawing.Size(75, 23);
            this.btn_Largest.TabIndex = 0;
            this.btn_Largest.Text = "button1";
            this.btn_Largest.UseVisualStyleBackColor = true;
            this.btn_Largest.Click += new System.EventHandler(this.btn_Largest_Click);
            // 
            // lbl_info
            // 
            this.lbl_info.AutoSize = true;
            this.lbl_info.Location = new System.Drawing.Point(13, 13);
            this.lbl_info.Name = "lbl_info";
            this.lbl_info.Size = new System.Drawing.Size(46, 17);
            this.lbl_info.TabIndex = 1;
            this.lbl_info.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 475);
            this.Controls.Add(this.lbl_info);
            this.Controls.Add(this.btn_Largest);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Largest;
        private System.Windows.Forms.Label lbl_info;
    }
}

