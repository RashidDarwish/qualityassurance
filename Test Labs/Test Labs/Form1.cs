﻿using System;
using System.Windows.Forms;
using KommunFeatures;

namespace Test_Labs
{
    public partial class Form1 : Form
    {
        private readonly KommunLista _kommuner;

        public Form1()
        {
            InitializeComponent();

            _kommuner = new KommunLista();
            lbl_info.Text = "Antal kommuner: " + _kommuner.Count();
        }

        private void btn_Largest_Click(object sender, EventArgs e)
        {
            var largest = _kommuner.Largest();
            MessageBox.Show("Den största kommunen är " + largest.Namn + " med " + largest.Invanare + " invånare.");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
    }
}