﻿using KommunFeatures;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KommunFeatures.Tests
{
    [TestClass()]
    public class TestKommunLista
    {
        /// <summary>
        ///     Detta är en kommentar. Första uppgiften går ut på att skriva kommentarer
        ///     för samtliga testfunktioner.
        /// </summary>
        [TestMethod()]
        public void TestKommunListaInitializationA()
        {
            var kl = new KommunLista();
            Assert.AreEqual(290, kl.Count());
        }

        [TestMethod]
        public void TestKommunListaInitializationB()
        {
            var kl = new KommunLista("KommunkoderB.csv");
            Assert.AreEqual(290, kl.Count());
        }

        [TestMethod]
        public void TestKommunListaInitializationC()
        {
            var kl = new KommunLista("Kommunkoder C.csv");
            Assert.AreEqual(290, kl.Count());
        }

        [TestMethod]
        public void TestKommunListaInitializationD()
        {
            var kl = new KommunLista("alternativkommunlista.csv");
            Assert.AreEqual(290, kl.Count());
        }

        [TestMethod]
        public void TestFindLargest()
        {
            var kl = new KommunLista();
            var k = kl.Largest();
            Assert.AreEqual("Stockholms stad", k.Namn);
        }

        [TestMethod]
        public void TestFindSmallest()
        {
            var kl = new KommunLista();
            var k = kl.Smallest();
            Assert.AreEqual("Bjurholms kommun", k.Namn);
        }

        [TestMethod]
        public void TestFindSmaKommunerA()
        {
            var kl = new KommunLista();
            var smaKommuner = kl.SmaKommuner();
            foreach (var k in smaKommuner)
                Assert.IsFalse(k.Invanare > 20000);
        }

        [TestMethod]
        public void TestFindSmaKommunerB()
        {
            //var kl = new KommunLista();
            //var smaKommuner = kl.SmaKommuner();
            //Assert.AreEqual(42, smaKommuner.Count());
        }

        [TestMethod]
        public void TestTotaltAntalInvanare()
        {
            var kl = new KommunLista();
            var total = kl.Invanare();
            Assert.IsTrue(total > 10000000);
        }
    }
}